variable "ec_stack_region" {
  type = string
}

variable "ec_deployment_id" {
  type = string
}

variable "resource_group_location" {
  type = string
}

variable "windows_admin_username" {
  type = string
}

variable "windows_admin_password" {
  type = string
}

variable "windows_host_size" {
  type = string
}

terraform {

  required_providers {
   ec = {
     source = "elastic/ec"
      version =  "0.2.1"
    }

     azurerm = {
      source = "hashicorp/azurerm"
      version =  "~>2.0"
          }
  }
}

provider azurerm {
  features {
  }
}

provider ec {

}

## Elastic cloud cluster
data "ec_stack" "latest" {
  version_regex = "latest"
  region        = "${var.ec_stack_region}"
}

resource "ec_deployment" "threat-hunting-cluster" {
  name = "threat-hunting-cluster"

  region = "${var.ec_stack_region}" 
  version  = data.ec_stack.latest.version
  deployment_template_id = "${var.ec_deployment_id}"

  elasticsearch {
     
     topology {
      id   = "hot_content"
      size = "8g"
    }

     topology {
      id   = "hot_content"
      size = "8g"
    }

    topology {
      id   = "hot_content"
      size = "8g"
    }

    topology {
      id   = "ml"
      size = "8g"
    }

  }

  kibana {}


}


## Azure networking and VMs
resource "azurerm_resource_group" "threat-hunting-lab" {
  name = "theat-hunting-lab" 
  location = var.resource_group_location
}

resource "azurerm_virtual_network" "hosts" {
  name                = "hosts"
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name 
  location            = var.resource_group_location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "subnet-01" {
  name                 = "subnet-01"
  resource_group_name  = azurerm_resource_group.threat-hunting-lab.name 
  virtual_network_name = azurerm_virtual_network.hosts.name
  address_prefixes     = ["10.0.2.0/24"]
}


## Windows host machine
resource "azurerm_public_ip" "windows-host-public-IP" {
  name                = "windows-host-public-IP"
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name
  location            = var.resource_group_location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "windows-host-nic" {
  name                = "windows-host-nic" 
  location            = var.resource_group_location
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet-01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.windows-host-public-IP.id

  }
}

resource "azurerm_windows_virtual_machine" "windows-host" {
  name                = "windows-host"
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name
  location            = var.resource_group_location
  size                = var.windows_host_size
  admin_username      = var.windows_admin_username
  admin_password      = var.windows_admin_password
  network_interface_ids = [
    azurerm_network_interface.windows-host-nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}


## Linux host machine
resource "azurerm_public_ip" "linux-host-public-IP" {
  name                = "linux-host-public-IP"
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name
  location            = var.resource_group_location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "linux-host-nic" {
  name                = "linux-host-nic" 
  location            = var.resource_group_location
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet-01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.linux-host-public-IP.id

  }
}

resource "azurerm_linux_virtual_machine" "linux-host" {
  name                = "linux-host"
  resource_group_name = azurerm_resource_group.threat-hunting-lab.name
  location            = azurerm_resource_group.threat-hunting-lab.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.linux-host-nic.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(".ssh/linux-host.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}

## Linux attacker
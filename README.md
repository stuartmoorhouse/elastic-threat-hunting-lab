# elastic-threat-hunting-lab

A lab for using the Elastic stack threat-hunting capabilities. Creates an Elastic Cloud cluster with a machine-learning node, host Windows and Linux machines and a Debian machine for attacks.



### Create ssh keys for the Linux machines
``mkdir .ssh``
``ssh-keygen -t rsa -b 4096 -f .ssh/linux-host``




